def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'server', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t tushar24sharma/docker:3 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push tushar24sharma/docker:3'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
